<?php
// -------------
// Evaluation PHP - Pratique
// --
// Exercice 1 : On se présente
// -------------

// -------------
// NOTATION
// -------------
//
// Array (1pt)
// 0.5pt    pour la déclaration d'un tableau
// 0.5pt    si le tableau est un tableau associatif simple
//
// Foreach + condition sur la date (2pts)
// 0.5pt    pour la présence du Foreach
// 0.5pt    pour la liste HTML DANS le Foreach
// 0.5pt    pour la présence de la condition DANS le foreach
// 0.5pt    pour afficher la date au format français
//
// Objet DateTime (bonus) (1pt)
// 0.5pt    pour l'utilisation de DateTime
// 0.5pt    pour l'utilisation de DateTime pour afficher la date au format français



// ETAPE 1
// -------------

// Déclaration du tableau, je l'appel "$user".
//
// Gardez toujours en tête qu'un tableau est UNE SOURCE DE DONNEES, et que
// celle-ci vient souvent d'une base de données ou les données possède déjà
// un certain formatage.
//
// De ce fait, on évite d'utiliser des fonctions ou de formater les données
// lors de la déclaration du tableau.
$user = [
  'firstname'   => 'John',
  'lastname'    => 'DOE',
  'address'     => '32 avenue de la République',
  'postalcode'  => '59110',
  'city'        => 'La Madeleine',
  'email'       => 'john.doe@webforce3.fr',
  'phone'       => '0601020304',
  'birthday'    => '2000-07-12' // "Date de naissance au format anglais (YYYY-MM-DD)"
];



// ETAPE 2
// -------------

// On demande l'affichage dans une liste HTML....
// >>>>> UNE LISTE, H.T.M.L !!!!!
// Donc on construit un DOCUMENT BIEN FORME !!!
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Exercice 1</title>
    </head>
    <body>
        <h2>Présentation</h2>

        <!-- On affiche le tableau dans une LISTE HTML -->
        <ul><!-- Début d ela balise LISTE -->



        <!--
        ETAPE 3
        Une boucle PHP pour parcourir le tableau $user.
        La meilleur pour ce job, c'est "foreach"
        -->
        <?php foreach ($user as $key => $value): ?>
            <li>
                <?php

                // On peut definir un label pour chaques clés ($key) du tableau
                switch ($key) {
                    case 'firstname':
                        $label = "Prénom";
                        break;

                    case 'lastname':
                        $label = "NOM";
                        break;

                    case 'address':
                        $label = "Adresse";
                        break;

                    case 'postalcode':
                        $label = "Code Postal";
                        break;

                    case 'city':
                        $label = "Ville";
                        break;

                    case 'email':
                        $label = "Email";
                        break;

                    case 'phone':
                        $label = "Téléphone";
                        break;
                        
                    // "case 'birthday'" reviens à faire une condition
                    // if ($key == "birthday") { ... }
                    case 'birthday':
                        $label = "Date de naissance";
                        $value = DateTime::createFromFormat('Y-m-d', $value)->format('m/d/Y');
                        // ------^^^^^^^^  Classe DatTime            ^^^^^^
                        //-------------------------------------------^^^^^^ Dans cette itération, $value vaut '2000-07-12'
                        // La méthode "->format(...)" permet de choisir le format de sortie
                        // la sortie de DateTime::createFromFormat(...)->format(...); est réaffecté à la variable $value
                        break;
                }

                // on Affiche les données
                echo $label." : ".$value;

                ?>

            </li><!-- Fin d ela balise LISTE -->

        <?php endforeach; ?>
        </ul>
    </body>
</html>
