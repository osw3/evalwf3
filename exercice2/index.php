<?php
// -------------
// Evaluation PHP - Pratique
// --
// Exercice 2 : On part en voyage
// -------------

// -------------
// NOTATION
// -------------
// structure d'une fonction (1pt)
// 0.5pt    pour la structure d'une fonction
// 0.5pt    pour un return
//
// 1er paramètre (1pt)
// 0.5pt    pour le 1er paramètre
// 0.5pt    pour le controle du 1er paramètre
//
// 2nd paramètre (1pt)
// 0.5pt    pour le 2nd paramètre
// 0.5pt    pour le controle du 2nd paramètre
//
// Calculs (2pts)
// 0.5pt    pour la présence du calcul EUR > USD
// 0.5pt    pour la présence du calcul USD > EUR
// 0.5pt    pour la l'utilisation des bonnes formules
// 0.5pt    pour la l'utilisation de constantes pour definir les valeurs de EUR et USD

// INFOS UTILE
// ----------
// Trouver la formule de convertion
// http://www.capte-les-maths.com/proportionnalite/change-des-monnaies.php



// On peut commencer par definir des constantes sur la base des informations
// que nous connaissons

// Valeur de l'€uro
define("EUR", 1);

// Valeur du Dollar US
define("USD", 1.085965);


// Déclaration de la fonction de convertion
// je l'appel "currenctConverter" et je lui donne deux arguments
// 1) $input qui prendra la valeur de la somme à convertir
// 2) $currency qui définira
function currencyConverter($input, $currency) {

    $currencyAvailable = ['EUR', 'USD'];

    // On definit la valeur de retoure par defaut
    $output = null;

    // Enoncé : "Le montant de type int ou float"
    // Donc SI le montant n'est pas du type attendu,
    // on retourne un message d'erreur
    if (!is_int($input) && !is_float($input)) {
        trigger_error("The amount must be an INTEGER or a FLOAT", E_USER_WARNING);
        return false;
    }

    // Enoncé : "La devise de sortie (uniquement EUR ou USD)"
    // Donc si la devise n'est pas du type attendu,
    // on retourne un message d'erreur
    if (!in_array($currency, $currencyAvailable)) {
        trigger_error("The currency must be ".implode(",", $currencyAvailable), E_USER_WARNING);
        return false;
    }

    // USD vers EUR
    // si $currency vaut EUR, on suppose que $input est une somme en Dollar
    // à convertir en Euro
    if ($currency == "EUR") {
        $output = (EUR * $input) / USD;
    }

    // EUR vers USD
    // si $currency vaut USD, on suppose que $input est une somme en Euro
    // à convertir en Dollar
    if ($currency == "USD") {
        $output = (USD * $input) / EUR;
    }

    // Pourquoi faire ces calculs :
    // (EUR * $input) / USD; et (USD * $input) / EUR;
    // alors que
    // EUR * $input; et USD * $input
    // aurait suffit ?
    //
    // Dans notre cas on estime que "1" EURO vaut "1.085965" USD
    // mais il se pourrait que "1" ne soit pas toujours notre base de calcul
    // En effet, si 1.085965 USD vaut 0,893455 EUR, la base de calcul, donc le
    // taux de conversion change
    return $output;
}

echo "1 EUR = ".currencyConverter(1, 'USD')." USD.<br />";
echo "42 EUR = ".currencyConverter(42, 'USD')." USD.<br />";
echo "20 USD = ".currencyConverter(20, 'EUR')." EUR.<br />";
?>
