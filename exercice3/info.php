<?php
// On inclus les ressources nécessaires au bon fonctionnement du programme
include_once 'init.php';

if (!isset($_GET['id'])) {
    die ("L'id du film n'est pas définit");
} else {
    $movie = getMovie($_GET['id']);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">
            <div class="page-header">
                <h1>Eval PHP <small>Exercice 3</small></h1>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <h3>Info du film</h3>

                    <dl class="dl-horizontal">

                        <dt>Titre</dt>
                        <dd><?php echo $movie->title; ?></dd>

                        <dt>Acteurs</dt>
                        <dd><?php echo $movie->actors; ?></dd>

                        <dt>Directeur</dt>
                        <dd><?php echo $movie->director; ?></dd>

                        <dt>Producteur</dt>
                        <dd><?php echo $movie->producer; ?></dd>

                        <dt>Année</dt>
                        <dd><?php echo $movie->year_of_prod; ?></dd>

                        <dt>Langue</dt>
                        <dd><?php echo $movie->language; ?></dd>

                        <dt>Categorie</dt>
                        <dd><?php echo $movie->category; ?></dd>

                        <dt>Synopsis</dt>
                        <dd><?php echo $movie->storyline; ?></dd>

                        <dt>Video</dt>
                        <dd><a href="<?php echo $movie->video; ?>" target="_blank"><?php echo $movie->video; ?></a></dd>

                    </dl>


                    <a href="list.php" class="btn btn-default">Retour à la liste</a>

                </div>
            </div>
        </div>

    </body>
</html>
