<?php
// On inclus les ressources nécessaires au bon fonctionnement du programme
include_once 'init.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">
            <div class="page-header">
                <h1>Eval PHP <small>Exercice 3</small></h1>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <h3>Liste des films</h3>

                    <table class="table">
                        <tr>
                            <th>Titre</th>
                            <th>Année</th>
                            <th>voir</th>
                        </tr>

                        <?php foreach (getMovies() as $movie): ?>
                        <tr>
                            <td><?php echo $movie->title; ?></td>
                            <td><?php echo $movie->year_of_prod; ?></td>
                            <td><a href="info.php?id=<?php echo $movie->id; ?>"><i class="glyphicon glyphicon-film"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </table>

                    <a href="index.php" class="btn btn-default">Retour à l'index</a>
                    <a href="add.php" class="btn btn-info pull-right">Ajouter un film</a>
                </div>
            </div>
        </div>

    </body>
</html>
