<?php
// NOTATION
// ----------
// Création base + table de données (fichier SQL fourni) (1pt)
// 0.20pt   pour la Fourniture du fichier
// 0.20pt   pour la Respect du nom de la BDD "exercice_3"
// 0.20pt   pour la Respect du nom de la table "movies"
// 0.20pt   pour la Structure de la BDD
// 0.20pt   pour le champ ID INT NN AI

// Vérifications formulaire (2pt)
// 0.10pt   Formulaire en POST
// 0.50pt   Controle d'envois du formulaire
// 0.10pt   Les champs select (année de production)
// 0.10pt   Les champs select (langue)
// 0.10pt   Les champs select (category)
// 0.10pt   Champ année généré en PHP
// 0.10pt   Controle de la longueur des champs (titre)
// 0.10pt   Controle de la longueur des champs (nom du réalisateur)
// 0.10pt   Controle de la longueur des champs (acteurs)
// 0.10pt   Controle de la longueur des champs (producteur)
// 0.10pt   Controle de la longueur des champs (synopsis)
// 0.50pt   Controle du champs Video (url)

// Affichage des erreurs ou réussite (1pt)
// 0.25pt   pour l'initialisation de la session
// 0.25pt   pour l'utilisation de la session
// 0.25pt   pour l'affichage des messages d'erreurs en rouge sous les champs
// 0.25pt   pour l'affichage des messages d'erreurs ou de réussite d'enregistrement en BDD, dans la flashbag

// Affichage liste des films (1pt)
// 0.25pt   pour l'utilisation d'une la requete SELECT
// 0.25pt   pour la seléction des champs title, realisateur, année, id
// 0.25pt   pour l'utilisation d'une boucle
// 0.25pt   pour les lignes du tableau dans une boucle

// Lien vers la fiche détail d'un film (1pt)
// 0.5pt    pour le lien html
// 0.5pt    pour le passage du parametre permettant d'identifier le film

// Détail d'un film (1pt)
// Condition sur $_GET (1pt)


// -----------------------------------------------------------------------------

// On inclus les ressources nécessaires au bon fonctionnement du programme
include_once 'init.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">
            <div class="page-header">
                <h1>Eval PHP <small>Exercice 3</small></h1>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <div class="list-group">
                        <a href="list.php" class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>&nbsp;&nbsp;&nbsp;Liste des films</a>
                        <a href="add.php" class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>&nbsp;&nbsp;&nbsp;Ajouter un film</a>
                    </div>

                </div>
            </div>
        </div>

    </body>
</html>
