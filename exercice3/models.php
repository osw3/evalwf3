<?php

function getCategories() {
    global $pdo;
    $sql = "SHOW COLUMNS FROM `movies` LIKE 'category'";
    $query = $pdo->query($sql);
    $result = $query->fetch(PDO::FETCH_ASSOC);

    $type = preg_replace("/'/", "", $result['Type']);
    preg_match("/enum\((.*)\)$/", $type, $matches);
    return explode(',', $matches[1]);
}


function getMovies() {
    global $pdo;
    $sql = "SELECT id, title, year_of_prod FROM `movies`";
    $query = $pdo->query($sql);
    $result = $query->fetchAll(PDO::FETCH_OBJ);
    return $result;
}


function getMovie($id) {
    global $pdo;
    $sql = "SELECT * FROM `movies` WHERE id=:IdMovie";
    $query = $pdo->prepare($sql);
    $query->bindParam(':IdMovie', $id, PDO::PARAM_INT);
    $query->execute();

    $result = $query->fetch(PDO::FETCH_OBJ);
    return $result;
}


function setMovie( $title, $actors, $director, $producer, $year_of_prod, $language, $category, $storyline, $video )
{
    global $pdo;

    // préparation de la requete
    $query = $pdo->prepare("INSERT INTO movies (`title`, `actors`, `director`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video`)
                            VALUES             (:title,  :actors,  :director,  :producer,  :year_of_prod,  :language,  :category,  :storyline,  :video)");

    // Bind value ou BindParam
    $query->bindParam(':title', $title, PDO::PARAM_STR);
    $query->bindParam(':actors', $actors, PDO::PARAM_STR);
    $query->bindParam(':director', $director, PDO::PARAM_STR);
    $query->bindParam(':producer', $producer, PDO::PARAM_STR);
    $query->bindParam(':year_of_prod', $year_of_prod, PDO::PARAM_STR);
    $query->bindParam(':language', $language, PDO::PARAM_STR);
    $query->bindParam(':category', $category, PDO::PARAM_STR);
    $query->bindParam(':storyline', $storyline, PDO::PARAM_STR);
    $query->bindParam(':video', $video, PDO::PARAM_STR);

    // execution de la requete
    $query->execute();
    $query->closeCursor();

    // On récupère et on retourne l'id de l'enregistrement
    // Si l'enregistrement échoue, PDO retourne false
    return $pdo->lastInsertId();
}
