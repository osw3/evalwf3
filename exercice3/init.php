<?php
include_once 'config.php';      // Config de l'application
include_once 'functions.php';   // Librairie de fonctions
include_once 'models.php';      // Fonctions CRUD movies


// Initialisation de la session php
session_start();

// On se connect à la base de données
try {
    $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$pass);
} catch (Exception $e) {
    var_dump($e);
    die('Erreur :' . $e->getMessage());
}
