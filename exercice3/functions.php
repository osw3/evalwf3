<?php

/**
 * formatStr
 * Protège et retourne une chaine de caratère
 * @param  [String]  $string Chaine d'entrée à protéger
 * @return [String]          chaine de sortie protégée
 */
function formatStr ($string) {
    $string = trim($string);
    $string = addslashes($string);
    $string = htmlspecialchars($string);
    return $string;
};

/**
 * printError
 * Affiche un message d'erreur en rouge (avec la class bootstrap "text-danger")
 * @param  [Array]  $errors Tableau des erreurs
 * @param  [String]  $field  Nom du champ pour lequel on souhaite récupérer le message d'erreur.
 */
function printError ($errors, $field) {
    foreach ($errors as $value) {
        if ( $value['field'] == $field ) {
            echo "<span class=\"text-danger\">".$value['message']."</span>";
        }
    }
}

/**
 * setFlashbag
 */
function setFlashbag($data) {

    // On vérifie l'existance de la Super Globale de $_SESSION
    // Si elle n'existe pas, on l'initialise
    if (!isset($_SESSION)) {
        session_start();
    }

    // On vérifie l'existance du tableau de messages de flashbag dans la $_SESSION
    // Si il n'existe pas, on l'initialise
    if (!isset($_SESSION['flashbag'])) {
        $_SESSION['flashbag'] = array();
    }

    // On ajoute le message à la liste des message du flashbag
    array_push($_SESSION['flashbag'], $data);
}

/**
 * getFlashbag
 */
function getFlashbag() {
    // On boucle sur la liste des message du flashbag
    foreach ($_SESSION['flashbag'] as $data) {

        // On affiche le message
        echo "<div class=\"alert alert-".$data['type']."\">"; // avec la class bootstrap correspondante au type du message (danger / success / ...)
        echo $data['message'];
        echo "</div>";

    }

    // On détruit le tableau de flashbag
    unset($_SESSION['flashbag']);
}
