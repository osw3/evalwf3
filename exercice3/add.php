<?php
// On inclus les ressources nécessaires au bon fonctionnement du programme
include_once 'init.php';


// On definie les valeur par défaut des champs
// Toutes les valeurs sont à NULL, en effet, rien n'ai saisie quand l'utilisateur
// arrive sur la page
$title          = null;
$actors         = null;
$director       = null;
$producer       = null;
$year_of_prod   = null;
$language       = null;
$category       = null;
$storyline      = null;
$video          = null;

// On créer un tableau ""$errors" qui nous servira au stockage des messages
// d'erreur si le formumaire contient des erreurs.
$errors = [];




// on controle l'envois du formulaire
//
// par défaut le tableau de la SuperGlobale $_POST est vide,
// on fait juste un controle qui nous permet de savoir si le
// tableau $_POST est vide ou non.
//
// Dans notre cas, on exécute le controle du fomulaire uniquement si le
// tableau $_POST n'EST PAS vide
if (!empty($_POST)) { // se lit : if $_POST is not empty.

    // On récupère les données du tableau $_POST, de cette façon,
    // on ne touche plus a la source des données (le $_POST)
	$title  		= $_POST['title'];
	$actors   		= $_POST['actors'];
	$director      	= $_POST['director'];
	$producer   	= $_POST['producer'];
	$storyline      = $_POST['storyline'];
	$year_of_prod   = $_POST['year_of_prod'];
	$language   	= $_POST['language'];
	$category   	= $_POST['category'];
	$video   		= $_POST['video'];

    // On peut se préocupper des injections SQL et attaques XSS en forcçant le
    // formatage des données grace a la fonction formatStr() du fichier fnc.php
    $title  		= formatStr($title);
    $actors   		= formatStr($actors);
    $director      	= formatStr($director);
    $producer   	= formatStr($producer);
    $storyline      = formatStr($storyline);
    $year_of_prod   = formatStr($year_of_prod);
    $language   	= formatStr($language);
    $category   	= formatStr($category);
    $video   		= formatStr($video);

    // On procède au controle des champs
    // --

    // controle des champs qui doivent contenir au minimum 5 caractères
	if (strlen($title) < 5 ) {
		array_push($errors, array(
			"field" => "title",
			"message" => "Le champ <strong>titre</strong> doit contenir au moins 5 caractères."
		));
	}
	if (strlen($actors) < 5 ) {
		array_push($errors, array(
			"field" => "actors",
			"message" => "Le champ <strong>acteurs</strong> doit contenir au moins 5 caractères."
		));
	}
	if (strlen($director) < 5 ) {
		array_push($errors, array(
			"field" => "director",
			"message" => "Le champ <strong>directeur</strong> doit contenir au moins 5 caractères."
		));
	}
	if (strlen($producer) < 5 ) {
		array_push($errors, array(
			"field" => "producer",
			"message" => "Le champ <strong>producteur</strong> doit contenir au moins 5 caractères."
		));
	}
	if (strlen($storyline) < 5 ) {
		array_push($errors, array(
			"field" => "storyline",
			"message" => "Le champ <strong>synopsis</strong> doit contenir au moins 5 caractères."
		));
	}

    // Controle de l'url de la vidéo
	if (!filter_var($video, FILTER_VALIDATE_URL)) {
		array_push($errors, array(
			"field" => "video",
			"message" => "Le champ <strong>video</strong> doit être une URL valide."
		));
	};


    // On controle le tableau d'erreurs
    // Si celui-ci est vide, on procède à l'enregistrement des données dans la BDD
    if (empty($errors)) {

        // On ajoute le film à la BDD
        // et on récupère la réponse dans la variable $movie.
        $movie = setMovie($title, $actors, $director, $producer, $year_of_prod, $language, $category, $storyline, $video);

        // Si l'enregistrement échou
        if ($movie === false) {
            // On créer un flashbag avec message d'erreur
            setFlashbag(array(
                "type" => "danger",
                "message" => "L'enregistrement du film en BDD à échoué."
            ));
        }

        // Si l'enregistrement réussi
        else {
            // On créer un flashbag avec le message de reussite
            setFlashbag(array(
                "type" => "success",
                "message" => "L'enregistrement du film en BDD à réussi."
            ));

            // On redirige l'utilisateur vers la fiche du film
            header("location: info.php?id=".$movie);
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">
            <div class="page-header">
                <h1>Eval PHP <small>Exercice 3</small></h1>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <h3>Ajouter un film</h3>

                    <form method="post">

                        <div class="row">
                            <div class="col-md-12">

                                <!-- Champ Titre du film -->
                                <div class="form-group">
                                    <label for="title">Titre du film</label>
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $title; ?>">
                	                <?php printError($errors, "title"); ?>
                                </div>

                                <!-- Champ Liste d'acteurs -->
                                <div class="form-group">
                                    <label for="actors">Liste d'acteurs</label>
                                    <input type="text" class="form-control" id="actors" name="actors" value="<?php echo $title; ?>">
                	                <?php printError($errors, "actors"); ?>
                                </div>

                                <!-- Champ Directeur -->
                                <div class="form-group">
                                    <label for="director">Directeur</label>
                                    <input type="text" class="form-control" id="director" name="director" value="<?php echo $title; ?>">
                	                <?php printError($errors, "director"); ?>
                                </div>

                                <!-- Champ Producteur -->
                                <div class="form-group">
                                    <label for="producer">Producteur</label>
                                    <input type="text" class="form-control" id="producer" name="producer" value="<?php echo $title; ?>">
                	                <?php printError($errors, "producer"); ?>
                                </div>

                            </div>

                            <div class="col-md-8">

                                <!-- Champ URL de la Vidéo -->
                                <div class="form-group">
                                    <label for="video">Video <small class="text-muted">url youtube (ex: https://youtu.be/5PSNL1qE6VY)</small></label>
                                    <input type="text" class="form-control" id="video" name="video" value="<?php echo $video; ?>">
                                    <?php printError($errors, "video"); ?>
                                </div>

                                <!-- Champ Synopsis -->
                                <div class="form-group">
                                    <label for="storyline">Synopsis</label>
                                    <textarea type="text" class="form-control" id="storyline" name="storyline" style="height: 108px;"><?php echo $storyline; ?></textarea>
                                    <?php printError($errors, "storyline"); ?>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <!-- Champ Année de production -->
                                <div class="form-group">
                                    <label for="year_of_prod">Année de production</label>
                                    <select class="form-control" id="year_of_prod" name="year_of_prod">
                                        <option value=""></option>
                                        <?php for ($i=date("Y"); $i >= 1900; --$i): ?>

                                            <option value="<?php echo $i; ?>"
                                                <?php

                                                // en cas d'erreur de saisi dans le formulaire
                                                // la ligne ci-dessous permet de selectionné
                                                // la date que l'utilisateur avait sélectionné
                                                // avant l'envois du formulaire
                                                echo ($i == $year_of_prod) ? "selected" : null;

                                                ?>
                                                ><!-- Chevron de fermeture de la balise ovrante <option> -->
                                                <?php echo $i; ?>
                                            </option>

                                        <?php endfor; ?>
                                    </select>
                                   <?php printError($errors, "year_of_prod"); ?>
                                </div>

                                <!-- Champ Choix de la langue -->
                                <div class="form-group">
                                    <label for="language">Langue</label>
                                    <select class="form-control" id="language" name="language">
                                        <option value=""></option>
                                        <option value="français">français</option>
                                        <option value="anglais">anglais</option>
                                    </select>
                                    <?php printError($errors, "language"); ?>
                                </div>

                                <!-- Champ Catégorie -->
                                <div class="form-group">
                                    <label for="category">Categorie</label>
                                    <select class="form-control" id="category" name="category">
                                        <option value=""></option>
                                        <?php foreach (getCategories() as $key => $value): ?>
                                        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php printError($errors, "category"); ?>
                                </div>

                            </div>
                        </div>

                        <a href="list.php" class="btn btn-default">Retour à la liste</a>
                        <button type="submit" class="btn btn-info pull-right">Enregistrer</button>

                    </form>


                </div>
            </div>
        </div>

    </body>
</html>
